/**
 * Позволяет отправлять код клиенту
 *
 * **Важно** реализовавыть настоящую отправку кода не нужно
 */
internal class CodeSender {
    /**
     * Отправляет код клиенту, чтобы он мог его ввести в форму для подтверждения входа
     * @param email почта клиента, которому необходимо отправить код
     * @param code код, который пользователь должен ввести в форму подтверждения
     */
     fun sendCode(email: String?, code: String?){
        println("in your $email was came SMS authorization code: $code")
    }
}
