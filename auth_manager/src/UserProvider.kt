/**
 * Абстрактный класс, который содержит в себе список пользователей. Указаные пользователи
 * в этой базе данных, могут успешно получить код для авторизации.
 *
 * **ВАЖНО** реализовывать базу данных для хранения пользователей не нужно!
 */

internal  class UserProvider {
    /**
     * Данный метод проверяет, если ли пользователь с таким email в нашей базе данных
     * @param email Почта клиента
     * @return Возвращает true, если удалось найти данного пользователя в базе данных, иначе false
     */
     fun hasUserByEmail(email: String?): Boolean{
        var findInDataBase = false
        if (email.equals("some email from data base")) {
            findInDataBase = true
        }
        return findInDataBase
    }
}
