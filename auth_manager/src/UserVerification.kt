import java.util.*
import kotlin.random.Random

fun main() {
    val userVerification = UserVerification()
    userVerification.authorization()
}
class UserVerification : Authorizator {
    fun authorization(){
        val scan = Scanner(System.`in`)
        //generation random code and send to user
        print("input email for generating code:")
        val email = scan.nextLine()
        val auth:EventAuth = this.generateCode(email)
        val codeSender = CodeSender()

        //imitation get SMS-code

        codeSender.sendCode(email, auth.code)

        print("input code: ")

        val authIsApprove = checkCode(email, scan.nextLine(), auth)
        if (authIsApprove) {
            print("Authorization is successful")
        }
        else print("Authorization is failed")
    }

    override fun generateCode(email: String):EventAuth {
        val randomValues = List(4) {Random.nextInt(0, 9)}
        val builder = StringBuilder()
        for (value in randomValues ){
            builder.append(value)
        }
        val code = builder.toString()
            //SENDING CODE TO USER
            return EventAuth(email, code)

    }
    override fun checkCode(email: String, code: String?, auth: EventAuth): Boolean {
        var isApprove = false
        if (email == auth.email
            && code==auth.code)
            isApprove = true
        return isApprove
    }

}

