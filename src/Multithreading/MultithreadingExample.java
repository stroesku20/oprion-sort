package Multithreading;

public class MultithreadingExample implements Increment {
    public int x = 0;

    public static void main(String[] args) throws InterruptedException {
        new MultithreadingExample().start();

    }

    public void start() throws InterruptedException {
        Thread myThread1 = new Thread(new MyThread1(this));
        myThread1.setName("1 -- ");

        Thread myThread2 = new Thread(new MyThread2(this));
        myThread2.setName("2 -- ");

        myThread1.start();
        myThread2.start();

        myThread1.join();
        myThread2.join();
        System.out.print("now X is ");
        System.out.println(x);

        System.out.print("now X is ");
        System.out.println(x);

        System.out.println(Thread.currentThread().getName() + " is stopped");
    }

    @Override
    public synchronized void increment() {
        x++;
    }
}

 interface  Increment {
     void increment();


     class MyThread1 implements Runnable {

         private final Increment increment;

         MyThread1(Increment increment) {
             this.increment = increment;
         }

         @Override
         public void run() {
             System.out.println(Thread.currentThread().getName() + " Hello ");
             for (int i = 0; i < 100000; i++) {
                 System.out.println(Thread.currentThread().getName() + " " + i);

                 increment.increment();
             }
             System.out.println("Thread 1 is stopped");
         }
     }

     class MyThread2 implements Runnable {

         private final Increment increment;

         MyThread2(Increment increment) {
             this.increment = increment;
         }

         @Override
         public void run() {
             System.out.println(Thread.currentThread().getName() + " Hello ");
             for (int i = 0; i < 100000; i++) {
                 System.out.println(Thread.currentThread().getName() + " " + i);
                 increment.increment();
             }
             System.out.println("Thread 2 is stopped");
         }
     }
 }