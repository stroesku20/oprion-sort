package Multithreading;
import java.io.*;
import java.util.*;

public class FileReaderExample {
    public static String text;
    public static FileWriter fw;
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        new Thread(new Thread1(new File(sc.nextLine()))).start();
    }


    static class Thread1 implements Runnable {
        private File file;

        public Thread1(File file) {
            this.file = file;
        }

        @Override
        public void run() {
            ArrayList<String> list = new ArrayList<>();
            try {
                if (file.exists()) {
                    BufferedReader reader = new BufferedReader(new FileReader(file));
                    while ((text = reader.readLine()) != null) {
                        if (text.isEmpty()) {
                            System.out.println("empty line");
                            return;
                        } else {
                            list.add(text);
                        }
                    }
                    System.out.println("Файл найден. Добавляем текст:");
                    fw = new FileWriter(file, true); //the true will append the new data
                    fw.write(sc.nextLine());//appends the string to the file
                    fw.close();
                    System.out.println(list);
                } else if (file.createNewFile()) {
                    System.out.println("Создаем новый файл...");
                    FileOutputStream fos = new FileOutputStream(file, true);

                    PrintWriter printWriter = new PrintWriter(fos);
                    System.out.println("Введите данные нового файла.");
                    printWriter.println(sc.nextLine());//appends the string to the file
                    printWriter.flush();
                    fos.close();
                    printWriter.close();
                }
            } catch (IOException ignored) {
            }
        }
    }
    public static void sum (File file){
        System.out.println(Thread.currentThread().getName());
        int integer;
        int sum = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            while ((text = reader.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(text);
                try {
                    while (st.hasMoreTokens()) {
                        integer = Integer.parseInt(st.nextToken());
                        sum = sum + integer;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("file contain illegal statement");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(sum);
    }
}



