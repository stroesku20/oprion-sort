

public class BinarySearch {

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 7, 9, 12, 34, 54, 67, 77};
        System.out.println(binarySearchRecursive(arr, 7, 0, arr.length - 1));

    }
    private static int binarySearchRecursive(int[] sortedArray, int key, int low, int high){
        int middle = low +(high-low)/2;
        if (low > high) {
            // не нашли элемента, который равен ключу
            return -1;
        }
        if (key == sortedArray[middle])
            return middle;
        else if (key > sortedArray[middle])
            return binarySearchRecursive(sortedArray,key, middle + 1, high);
        else
            return binarySearchRecursive(sortedArray, key, low, middle - 1);
    }
    private static int binarySearch(int[] array, int key){

         int low = 0; //начало подмассива
         int high = array.length - 1; //конец подмассива
            while (low <= high){
                int middle = low + (high - low)/2;  //находим середину

                if (key < array[middle]){   //если ключевое значение меньше середины
                    high = middle - 1;      //то середина становится концом подмассива (т.е) сокращаем массив вдвое и сдвигаем индекс на 1
                }
                else if (key > array[middle]){ // если ключевое значение больше середины
                    low = middle + 1;          //середина становится нижней частью подмассива и индекс сдвигаем на один
                } else return middle;          //в конечном итоге возвращаем середину которая будет равна искомому значению (key)
            }
        return -1;
    }
}
