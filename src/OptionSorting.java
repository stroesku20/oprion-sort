import java.util.Arrays;
import java.util.Scanner;

public class OptionSorting {
    public static Scanner sc = new Scanner(System.in);
    public static int sizeArray;
    public static int [] array;

    public static void main(String[] args) {

        System.out.println("Введите размер массива ");
        sizeArray = sc.nextInt();
        array = createArray(sizeArray);
        chooseOptionSorting();
        System.out.println(Arrays.toString(array));

    }

    private static int[] createArray(int sizeArray) {
        System.out.println("Введите " + sizeArray + " чисел");
        int[] intArray = new int[sizeArray];
        for (int i = 0; i < sizeArray; i++) {
            intArray[i] = sc.nextInt();
        }
        return intArray;
    }

    private static void chooseOptionSorting() {
        System.out.println("Введите 1 если сортировка Пузырьком");
        System.out.println("Введите 2 если сортировка Выбором");
        System.out.println("Введите 3 если сортировка Вставками");
        int x = sc.nextInt();
        if (x == 1) bubbleSort(array);
        if (x == 2) selectionSort(array);
        if (x == 3) insertionSort(array);
    }

    private static  int[] bubbleSort(int[] arr) {
        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    isSorted = false;
                    temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
        return array;
    }
    private static  int[] selectionSort(int[] array) {
        for (int left = 0; left < array.length; left++) {
            int minInd = left;
            for (int i = left; i < array.length; i++) {
                if (array[i] < array[minInd]) {
                    minInd = i;
                }
            }
            swap(array, left, minInd);
        }
        return array;
    }
    private static int[] insertionSort(int[] array) {
        for (int left = 0; left < array.length; left++) {
            int value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
        return array;
    }



    private static void swap(int[] array, int left, int minInd) {
            int tmp = array[left];
            array[left] = array[minInd];
            array[minInd] = tmp;
        }
    }
