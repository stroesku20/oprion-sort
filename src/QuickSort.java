import java.util.Arrays;

public class QuickSort {
    public static void main (String[]args){
        int[] x = {8, 0, 4, 7, 3, 7, 10, 12, -3};
        System.out.println("Было");
        System.out.println(Arrays.toString(x));

        int left = 0;
        int right = x.length - 1;

        quickSort(x, left, right);
        System.out.println("Стало");
        System.out.println(Arrays.toString(x));
    }
    public static void quickSort(int[] array, int left, int right) {

        if (array.length == 0)
            return;//завершить выполнение, если длина массива равна 0

        if (left >= right)
            return;//завершить выполнение если уже нечего делить

        // выбрать опорный элемент
        int middle = left + (right - left) / 2;
        int pivot = array[middle];

        // разделить на подмассивы, который больше и меньше опорного элемента
        int l = left, r = right;
        while (l <= r) {
            while (array[l] < pivot) {
                l++;
            }

            while (array[r] > pivot) {
                r--;
            }

            if (l <= r) {//меняем местами
                int temp = array[l];
                array[l] = array[r];
                array[r] = temp;
                l++;
                r--;
            }
        }

        // вызов рекурсии для сортировки левой и правой части
        if (left < r)
            quickSort(array, left, r);

        if (right > l)
            quickSort(array, l, right);
    }
}