package CarAccounting;

import CarAccounting.Models.Car;

import java.io.IOException;
import java.util.ArrayList;

public class CarManager {

    private  ArrayList<Car> cars;
    private CarsRepository carRepository;

    //Constructor
    public CarManager(ArrayList<Car> cars, CarsRepository carsRepository) {
        this.cars = cars;
        this.carRepository = carsRepository;
    }

    public void add(Car car) throws IOException {

        cars.add(car);
        carRepository.save(cars);
    }

    public void remove(int index) {
        if (cars != null && index > 0 && index <= cars.size()) {
            cars.remove(index);
        }
    }


    public ArrayList<Car> getCars() {
        return cars;
    }
}
