package CarAccounting;
import CarAccounting.Models.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Market implements Creator {

    public Scanner sc = new Scanner(System.in);
    public ArrayList<Car> cars = new ArrayList<>();
    CarsRepository carsRepository;
    CarManager carManager;
    CarsFactory carsFactory = new CarsFactory(this);
    Car car;
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Добро пожаловать в наш магазин, Вас интересуют новые авто, или подержанные?");
        Market market = new Market();
        File carListFile;
        if (sc.nextLine().contains("Новые")){
            carListFile = new File("carList.txt");
        } else {
            carListFile = new File("usedCarList.txt");
        }
         market.start(carListFile);
    }

    public void start(File file) throws IOException {
        carsRepository = new CarsRepository(file);
         carManager = new CarManager(cars, carsRepository);
        //choiceAndAdd(carManager);
        //cars = carsRepository.load();
        choiceEvent();

    }
    public void choiceEvent() throws IOException {
        System.out.println("Выберите дальнейшее действие:");
        System.out.println("Добавить");
        System.out.println("Удалить");
        System.out.println("Сортировать");
        System.out.println("Показать");
        System.out.println("Завершить");
        String s = sc.nextLine();

        if (s.equals("Завершить")){
            System.out.println("Программа завершена");
        }
        else  {
            switch (s) {
                case "Удалить":
                    System.out.println("Введите номер строки которую хотите удалить");
                    cars.clear();
                    cars = carsRepository.load();
                    print(cars);
                    carsRepository.remove(sc.nextInt() - 1);
                    break;
                case "Добавить":
                    addNewCar(carManager);
                    break;
                case "Сортировать":
                    cars.clear();
                    carsRepository.load();
                    carsRepository.sort();
                    carsRepository.refreshFile(cars);
                    break;
                case "Показать":
                    cars.clear();
                    cars = carsRepository.load();
                    print(cars);
                    break;
            }
            choiceEvent();
        }
    }

    public void addNewCar(CarManager carManager)  throws IOException {
        System.out.println("Введите категорию авто");
        Car car = carsFactory.createCar(EnumCar.getEnumByType(sc.nextLine()));
        carManager.add(car);
        }



    public void print(ArrayList<Car> cars){
        for (Car car : cars) {
            System.out.println(car);
        }
    }

    @Override
    public Car createCargoCar() {
        System.out.println("Вводите через запятую: модель, максимальную скорость, цену($), грузоподъемность");
        String text = sc.nextLine();
        String[] tokensFromLine = text.split(",");
        String carModel = tokensFromLine[0];
        int maxSpeed = Integer.parseInt(tokensFromLine[1]);
        int price = Integer.parseInt(tokensFromLine[2]);
        int badge = Integer.parseInt(tokensFromLine[3]);
        car = new CargoCar(carModel, maxSpeed, price, badge );
        return car;
    }

    @Override
    public Car createPassengerCar() {
        System.out.println("Вводите через запятую: модель, максимальную скорость, цену($), вместимость ");
        String text = sc.nextLine();
        String[] tokensFromLine = text.split(",");
        String carModel = tokensFromLine[0];
        int maxSpeed = Integer.parseInt(tokensFromLine[1]);
        int price = Integer.parseInt(tokensFromLine[2]);
        int capacity = Integer.parseInt(tokensFromLine[3]);
        car = new PassengerCar(carModel, maxSpeed, price, capacity );
        return car;
    }
    @Override
    public Car createRacingCar() {
        System.out.println("Вводите через запятую: модель, максимальную скорость, цену($), мощность (л), разгон до 100км/ч");
        String text = sc.nextLine();
        String[] tokensFromLine = text.split(",");
        String carModel = tokensFromLine[0];
        int maxSpeed = Integer.parseInt(tokensFromLine[1]);
        int price = Integer.parseInt(tokensFromLine[2]);
        int power = Integer.parseInt(tokensFromLine[3]);
        int boostTo100 = Integer.parseInt(tokensFromLine[4]);
        car = new RacingCar(carModel, maxSpeed, price, power, boostTo100 );
        return car;
    }
    @Override
    public Car createSportCar() {
        System.out.println("Вводите через запятую: модель, максимальную скорость, цену($), мощность (л) ");
        String text = sc.nextLine();
        String[] tokensFromLine = text.split(",");
        String carModel = tokensFromLine[0];
        int maxSpeed = Integer.parseInt(tokensFromLine[1]);
        int price = Integer.parseInt(tokensFromLine[2]);
        int boostTo100 = Integer.parseInt(tokensFromLine[3]);
        car = new SportCar(carModel, maxSpeed, price, boostTo100 );
        return car;
    }

//    public static void refresh(File file) throws IOException {
//        FileWriter fileWriter = new FileWriter(file);
//        for (Car car : cars) {
//            fileWriter.write(car.toString()+"\n");
//        }
//        fileWriter.close();
//    }
}
