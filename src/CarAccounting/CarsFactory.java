package CarAccounting;

import CarAccounting.Models.*;

import java.util.Scanner;
//Фабрика должна создавать car и при вводе с консоли, и при считывании из файла
public class CarsFactory {
    Creator creator;

    public CarsFactory(Creator creator) {
        this.creator = creator;
    }

    public Car createCar(EnumCar type) {
        Car car;
        switch (type) {
            case CARGO_CAR:
                car = creator.createCargoCar();
                break;
            case PASSENGERS_CAR:
                car = creator.createPassengerCar();
                break;
            case RACING_CAR:
                car = creator.createRacingCar();
                break;
            case SPORT_CAR:
                car = creator.createSportCar();
                break;
            default:
                throw new RuntimeException("category unknown");

        }

        return car;
    }
}

