package CarAccounting.Models;

import CarAccounting.EnumCar;

public class PassengerCar extends Car {
    public int capacityMax;
    public EnumCar carType = EnumCar.PASSENGERS_CAR;

    public PassengerCar(String carModel, int speedMax, int price, int capacityMax) {
        super(carModel, speedMax, price);
        this.capacityMax = capacityMax;
    }
    public String getCarType() {
        return carType.getText();
    }
    public int getCapacityMax() {
        return capacityMax;
    }

    @Override
    public String toString() {
        return carType +"," +
                carModel +"," +
                speedMax +"," +
                price +"," +
                capacityMax;
    }
}
