package CarAccounting.Models;

import CarAccounting.EnumCar;

public class SportCar extends Car {
    public int boostTo100;
    public EnumCar carType = EnumCar.SPORT_CAR;
    public SportCar(String carModel, int speedMax, int price, int boostTo100) {
        super(carModel,  speedMax, price);
        this.boostTo100 = boostTo100;
    }

    public int getBoostTo100() {
        return boostTo100;
    }

    public String getCarType() {
        return carType.getText();
    }
    @Override
    public String toString() {
        return  carType +"," +
                carModel +"," +
                speedMax +"," +
                price +"," +
                boostTo100;

    }
}
