package CarAccounting.Models;

import CarAccounting.EnumCar;

public class RacingCar extends Car {
    public EnumCar carType = EnumCar.RACING_CAR;
    public int power;


    public int boostTo100;

    public RacingCar( String carModel, int speedMax, int price, int power, int boostTo100) {
        super( carModel,  speedMax, price);
        this.power = power;
        this.boostTo100 = boostTo100;
    }

    public String getCarType() {
        return carType.getText();
    }
    public int getPower() {
        return power;
    }

    public int getBoostTo100() {
        return boostTo100;
    }

    @Override
    public String toString() {
        return  carType +"," +
                carModel +"," +
                speedMax +"," +
                price +"," +
                power;
    }
}
