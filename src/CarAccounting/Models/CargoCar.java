package CarAccounting.Models;

import CarAccounting.EnumCar;

public class CargoCar extends Car {
    public EnumCar carType = EnumCar.CARGO_CAR;
    public int baggageMax;

    public CargoCar(String carModel, int speedMax, int price, int baggageMax) {
        super(carModel, speedMax, price);
        this.baggageMax = baggageMax;
    }

    public String getCarType() {
        return carType.getText();
    }

    public int getBaggageMax() {
        return baggageMax;
    }

    @Override
    public String toString() {
        return  carType +"," +
                carModel +"," +
                speedMax +"," +
                price +"," +
                baggageMax;

    }


}
