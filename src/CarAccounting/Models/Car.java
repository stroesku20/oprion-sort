package CarAccounting.Models;

import CarAccounting.EnumCar;

public  class Car {

    public String carModel;
    public int speedMax;
    public int price;

    public Car(String carModel, int speedMax, int price) {
        this.carModel = carModel;
        this.speedMax = speedMax;
        this.price = price;
    }
    public String getCarType(){
    return null;
    }

    public String getCarModel() {
        return carModel;
    }

    public int getSpeedMax() {
        return speedMax;
    }

    public int getPrice() {
        return price;
    }
}
