package CarAccounting;

import CarAccounting.Models.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.StringTokenizer;

public class CarsRepository implements Creator{

    private ArrayList<Car> cars = new ArrayList<>();
    File file;
    StringTokenizer stringTokenizer;
    String[] tokensFromLine;
    EnumCar type;
    public CarsRepository(File file){
        this.file = file;
    }

    //запись cars в file
    public void save(ArrayList<Car> cars) throws IOException {
        FileWriter fileWriter = new FileWriter(file, true);
        for (Car car : cars)
                fileWriter.write(car.toString() + "\n");
        fileWriter.close();
    }

    public void sort() {
        Car[] carsArr = new Car[cars.size()];
        for (int i = 0; i <cars.size(); i++) {
            carsArr[i] = cars.get(i);
        }
        quickSort(carsArr, 0, carsArr.length-1);
        cars.clear();
        Collections.addAll(cars, carsArr);
    }

    //сортировка
    public void quickSort(Car[] arr, int low, int high) {

        if (arr == null || arr.length == 0) return;
        if (low >= high)  return;
        // pick the pivot
        int middle = low + (high - low) / 2;
        Car pivot = arr[middle];
        // make left < pivot and right > pivot
        int i = low, j = high;
        while (i <= j) {
            while (arr[i].getSpeedMax() < pivot.getSpeedMax()) {
                i++;
            }
            while (arr[j].getSpeedMax() > pivot.getSpeedMax()) {
                j--;
            }
            if (i <= j) {
                Car temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
            }
        }
        // recursively sort two sub parts
        if (low < j) {
            quickSort(arr, low, j);
        }
        if (high > i) {
            quickSort(arr, i, high);
        }
    }

    //выгрузка из файла
    public ArrayList<Car> load() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String text;
        CarsFactory factory = new CarsFactory(this);
        while ((text = reader.readLine()) != null) {
            stringTokenizer = new StringTokenizer(text,",");
            tokensFromLine = text.split(",");
            type = EnumCar.getEnumByType(tokensFromLine[0]);
            Car car = factory.createCar(type);
            cars.add(car);
        }return cars;
    }

    public String next() {
        return stringTokenizer.nextToken();
    }

    public int nextInt() {
        return Integer.parseInt((next()));
    }

    @Override
    public Car createCargoCar() {
        next();
        return new CargoCar(next(),nextInt(), nextInt(),nextInt());
    }

    @Override
    public Car createPassengerCar() {
        next();
        return new PassengerCar(next(),nextInt(), nextInt(),nextInt());
    }

    @Override
    public Car createRacingCar() {
        next();
        return new RacingCar(next(),nextInt(), nextInt(),nextInt(),nextInt());
    }

    @Override
    public Car createSportCar() {
        next();
        return new SportCar(next(),nextInt(), nextInt(),nextInt());
    }
    public void remove(int index) throws IOException {
        if (cars == null
                || index < 0
                || index >= cars.size()) {
            System.out.println(cars);
        } else {
            System.out.println("Элемент "+cars.get(index)+" удален");
            System.out.println();
            cars.remove(index);
            refreshFile(cars);

        }
    }
    public void refreshFile(ArrayList<Car> cars) throws IOException {
        FileWriter fileWriter = new FileWriter(file,false);
        for (Car car : cars)
            fileWriter.write(car.toString() + "\n");
        fileWriter.close();
    }

}
