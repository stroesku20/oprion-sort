package CarAccounting;

import CarAccounting.Models.*;

import java.io.IOException;

public interface Creator {

       Car createCargoCar();
       Car createPassengerCar();
       Car createRacingCar();
       Car createSportCar();

}
