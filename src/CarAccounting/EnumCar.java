package CarAccounting;
public enum EnumCar {
    CARGO_CAR("Грузовое авто"),
    PASSENGERS_CAR("Легковое авто"),
    RACING_CAR("Гоночное авто"),
    SPORT_CAR("Спортивное авто");

    private final String text;

    EnumCar(final String text) {
        this.text = text;
    }
    public static EnumCar getEnumByType(String type){
        for (EnumCar value:values()) {
            if (value.text.equals(type)) {
                return value;
            }
        }
        throw new  RuntimeException("cannot find type(" + type);
    }


    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return text;
    }
}